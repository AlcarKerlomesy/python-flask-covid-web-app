# After running this code, don't forget to indicate in covid_web_app/config.py where the generated file is

from numpy import dtype, array, save

var_dict = {
    'FLASK_SECRET_KEY': 'some key',
    'FLASK_DATABASE': 'sqlite:///site.db',
    'FLASK_EMAIL_SERVER': 'smtp.some_server.com',
    'FLASK_EMAIL_PORT': 'some number',
    # With a string without the @, the web app will block all the functionalities that send an email to the users
    'FLASK_EMAIL_USER': 'some_address@somewhere.com',
    'FLASK_EMAIL_PASS': 'some password'
}

var_dtype = dtype({'names': list(var_dict),
                   'formats': [f'U{len(var_str)}' for var_str in var_dict.values()]})

var_array = array(tuple(var_dict.values()), var_dtype)
save('config_var.npy', var_array, allow_pickle=False)
