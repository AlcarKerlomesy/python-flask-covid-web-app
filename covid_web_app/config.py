from numpy import load

config_dict = load(r'\Path\To\config_var.npy')
python_ver = 3.9


class Config:
    SECRET_KEY = str(config_dict['FLASK_SECRET_KEY'])
    BCRYPT_HANDLE_LONG_PASSWORDS = True

    SQLALCHEMY_DATABASE_URI = str(config_dict['FLASK_DATABASE'])
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    MAIL_SERVER = str(config_dict['FLASK_EMAIL_SERVER'])
    MAIL_PORT = int(config_dict['FLASK_EMAIL_PORT'])
    MAIL_USE_TLS = True
    MAIL_USERNAME = str(config_dict['FLASK_EMAIL_USER'])
    MAIL_PASSWORD = str(config_dict['FLASK_EMAIL_PASS'])
