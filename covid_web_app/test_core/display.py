from ..core import user_get_info, rs_get_info
import matplotlib.pyplot as plt
import numpy as np


def plot_state(user_list, rs_list, dot_pos=None):
    fig_h = plt.figure()
    ax_h = fig_h.add_subplot(projection='3d')

    if dot_pos is None:
        dot_pos = np.random.rand(3, len(user_list))

    dot_id = 0
    dot_color = ['g']*len(user_list)
    do_plot = [True]*len(user_list)
    for user_id in range(len(user_list)):
        if user_list[user_id]:
            user_info = user_get_info(user_list[user_id].user_state)
            if user_info.has_covid:
                dot_color[dot_id] = 'r'
            elif user_info.covid_distance == 1:
                dot_color[dot_id] = 'orange'
            elif user_info.covid_distance == 2:
                dot_color[dot_id] = 'y'
            elif user_info.covid_distance == 3:
                dot_color[dot_id] = 'c'
            dot_id += 1
        else:
            do_plot[user_id] = False

    ax_h.scatter(dot_pos[0, do_plot], dot_pos[1, do_plot], dot_pos[2, do_plot], color=dot_color[:dot_id])

    for cur_rs in rs_list:
        if cur_rs:
            cur_ids = (cur_rs.user_1_id, cur_rs.user_2_id)
            cur_color = 'black'
            rs_info = rs_get_info(cur_rs.rs_state)
            if rs_info.inactive:
                ax_h.plot3D(dot_pos[0, cur_ids], dot_pos[1, cur_ids], dot_pos[2, cur_ids], color=cur_color, linewidth=0.5, linestyle=(0, (5, 5)))
            else:
                user_info_1 = user_get_info(cur_rs.user_1.user_state)
                user_info_2 = user_get_info(cur_rs.user_2.user_state)

                if user_info_1.has_covid or user_info_2.has_covid:
                    cur_color = 'r'
                elif user_info_1.covid_distance == 1 or user_info_2.covid_distance == 1:
                    cur_color = 'orange'
                elif user_info_1.covid_distance == 2 or user_info_2.covid_distance == 2:
                    cur_color = 'c'

                ax_h.plot3D(dot_pos[0, cur_ids], dot_pos[1, cur_ids], dot_pos[2, cur_ids], color=cur_color, linewidth=0.5)

    for cur_id in range(len(user_list)):
        if user_list[cur_id]:
            ax_h.text(dot_pos[0, cur_id], dot_pos[1, cur_id], dot_pos[2, cur_id],
                      "  " + user_list[cur_id].username + " (" + str(cur_id) + ")", None, fontsize=6)

    ax_h.set_box_aspect((1, 1, 1))
    plt.axis('off')
    plt.show()
