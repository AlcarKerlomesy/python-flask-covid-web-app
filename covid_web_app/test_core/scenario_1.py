from itertools import combinations
import numpy as np
from .name_bank import get_name
from .manage_id import init_id
from .insertion_extraction import insert_rs as test_insert_rs, insert_user as test_insert_user, \
    delete_rs as test_delete_rs, extract_user as test_extract_user
from ..core import create_user, create_relationship, user_set_state, daily_update, delete_relationship, delete_user


def generate_user():
    user_list = []

    # 30 users
    id_list = [173, 29, 18, 164, 28, 195, 163, 96, 186, 143, 10, 5, 116, 1, 199,
               152, 31, 49, 99, 37, 16, 50, 38, 100, 177, 44, 94, 167, 57, 149]
    postal_code_list = [2233, 6697, 3030, 8016, 4186, 3153, 6750, 8972, 9931, 6954, 6460, 5022, 8564, 1054, 2799,
                        9710, 4669, 1723, 4152, 9700, 5263, 2869, 6648, 3659, 7010, 4198, 7701, 5267, 8149, 8411]
    psw = "Something"

    for (cur_id, cur_postal_code) in zip(id_list, postal_code_list):
        cur_name = get_name(cur_id)
        user_list.append(create_user(cur_name, cur_name + "@blo.com", cur_postal_code, psw))

    return user_list


def generate_relationship(user_list):
    rs_list = []

    groups = ((7, 12, 18, 20, 22, 29),
              (3, 8, 14, 17, 23),
              (2, 5, 8, 16, 22, 27),
              (1, 3, 13, 15, 21, 27),
              (0, 9, 13, 18, 24, 25, 28))
    for cur_group in groups:
        for cur_pair in combinations(cur_group, 2):
            rs_list.append(create_relationship(user_list[cur_pair[0]], user_list[cur_pair[1]]))

    line_rs = (9, 19, 10, 4, 26, 29)
    for cur_ind in range(1, len(line_rs)):
        rs_list.append(create_relationship(user_list[line_rs[cur_ind-1]], user_list[line_rs[cur_ind]]))

    networkers = (6, 11)
    networks = ((2, 5, 7, 8, 11, 13, 14, 18, 20, 23, 27),
               (0, 3, 5, 9, 12, 15, 16, 20, 22, 24, 28))

    for (cur_networker, cur_network) in zip(networkers, networks):
        for cur_rs in cur_network:
            rs_list.append(create_relationship(user_list[cur_networker], user_list[cur_rs]))

    return rs_list


def generate_dot_position(num_dots):
    dot_pos = np.zeros((3, num_dots))
    num_sum = np.zeros(num_dots)

    cube_size = (0.8, 0.2)
    cube_start = (1 - cube_size[0])/2
    group_pos = np.array(((0, 0, 1), (1, 1, 0), (1, 0, 1), (1, 0, 0), (0, 1, 0)))

    groups = ((7, 12, 18, 20, 22, 29),
              (3, 8, 14, 17, 23),
              (2, 5, 8, 16, 22, 27),
              (1, 3, 13, 15, 21, 27),
              (0, 9, 13, 18, 24, 25, 28))
    member_pos = (np.array(((-1, -1, 1), (1, 1, 1), (1, 1, -1), (-1, -1, -1), (1, -1, 1), (-1, 1, -1))),
                  np.array(((1, -1, -1), (1, -1, 1), (-1, -1, -1), (1, 1, -1), (-1, 1, 1))),
                  np.array(((-1, 1, -1), (-1, 1, 1), (1, 1, -1), (1, -1, 1), (-1, -1, 1), (1, -1, -1))),
                  np.array(((-1, -1, -1), (1, 1, -1), (-1, 1, -1), (-1, -1, 1), (1, 1, 1), (1, -1, 1))),
                  np.array(((1, -1, -1), (-1, -1, 1), (1, -1, -1), (1, -1, 1), (1, 1, -1), (-1, 1, 1), (1, 1, 1))))

    for group_id in range(len(groups)):
        dot_pos[:, groups[group_id]] += cube_start + (cube_size[0] * group_pos[group_id] +
                                                      cube_size[1] * member_pos[group_id] / 2).T
        num_sum[(groups[group_id],)] += 1

    dot_pos[:, num_sum > 1] /= num_sum[num_sum > 1]

    line_rs = (9, 19, 10, 4, 26, 29)

    add_vec = (dot_pos[:, (line_rs[-1],)] - dot_pos[:, (line_rs[0],)])/(len(line_rs) - 1)
    dot_pos[:, line_rs[1:-1]] = dot_pos[:, (line_rs[0],)] + add_vec*np.reshape(np.arange(1, len(line_rs) - 1), (1, -1))

    networkers = (6, 11)
    off_center = 0.1

    dot_pos[:, networkers] = np.ones((3, len(networkers)))/2
    dot_pos[:, networkers] += off_center*np.tile(np.array((-1, 1)), (3, 1))

    return dot_pos


def find_relationship(user, target_id):
    for cur_rs in user.relationships_1:
        if cur_rs.user_2_id == target_id:
            return cur_rs

    for cur_rs in user.relationships_2:
        if cur_rs.user_1_id == target_id:
            return cur_rs

    return None


def load_scenario():
    user_list = generate_user()
    rs_list = generate_relationship(user_list)
    init_id(user_list, rs_list)
    dot_pos = generate_dot_position(len(user_list))

    # Sub-scenario 1
    sick_user_id = 18
    test_user_id = 6

    # Sub-scenario 2
    # sick_user_id = 26
    # test_user_id = 29

    yield user_list, rs_list, dot_pos

    user_set_state(user_list[sick_user_id], has_covid=True, is_sick=False, has_recovered=False, is_vaccinated=False)

    yield user_list, rs_list, dot_pos

    cur_rs = find_relationship(user_list[sick_user_id], test_user_id)
    if cur_rs is None:
        raise Exception("Scenario 1: there is no relationship between " +
                        user_list[sick_user_id].username + " (" + str(sick_user_id) + ") and " +
                        user_list[test_user_id].username + " (" + str(test_user_id) + ")")
    delete_relationship(cur_rs)
    del_user_list, del_rs_list = daily_update(user_list, rs_list)
    test_delete_rs(rs_list, del_rs_list[0].id)

    yield user_list, rs_list, dot_pos

    test_insert_rs(rs_list, create_relationship(user_list[sick_user_id], user_list[test_user_id]))

    yield user_list, rs_list, dot_pos

    del_user = delete_user(user_list[test_user_id], [])
    del_user, del_dot = test_extract_user(user_list, rs_list, dot_pos, del_user.id)

    yield user_list, rs_list, dot_pos

    test_insert_user(user_list, dot_pos, del_user, del_dot)
    test_insert_rs(rs_list, create_relationship(user_list[sick_user_id], user_list[test_user_id]))

    yield user_list, rs_list, dot_pos
