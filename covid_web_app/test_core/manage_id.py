from datetime import date


def init_id(user_list, rs_list):
    for cur_id in range(len(user_list)):
        user_list[cur_id].id = cur_id

    for cur_id in range(len(rs_list)):
        rs_list[cur_id].id = cur_id
        rs_list[cur_id].user_1_id = rs_list[cur_id].user_1.id
        rs_list[cur_id].user_2_id = rs_list[cur_id].user_2.id
        if rs_list[cur_id].last_time is None:
            rs_list[cur_id].last_time = date.today()

    for cur_id in range(len(user_list)):
        if user_list[cur_id].parent:
            user_list[cur_id].parent_id = user_list[cur_id].parent.id
