import numpy as np
from datetime import date


def insert_rs(rs_list, ins_rs):
    ins_rs.user_1_id = ins_rs.user_1.id
    ins_rs.user_2_id = ins_rs.user_2.id
    if ins_rs.last_time is None:
        ins_rs.last_time = date.today()

    for rs_id in range(len(rs_list)):
        if rs_list[rs_id] is None:
            ins_rs.id = rs_id
            rs_list[rs_id] = ins_rs
            break
    else:
        ins_rs.id = len(rs_list)
        rs_list.append(ins_rs)


def delete_rs(rs_list, rs_id):
    rs_list[rs_id].user_1 = None
    rs_list[rs_id].user_2 = None
    rs_list[rs_id] = None


def insert_user(user_list, dot_pos, ins_user, ins_dot):
    if ins_user.parent:
        ins_user.parent_id = ins_user.parent.id

    for user_id in range(len(user_list)):
        if user_list[user_id] is None:
            ins_user.id = user_id
            user_list[user_id] = ins_user
            dot_pos[:, user_id] = ins_dot
            break
    else:
        ins_user.id = len(user_list)
        user_list.append(ins_user)
        dot_pos = np.column_stack((dot_pos, ins_dot))

    return dot_pos


def extract_user(user_list, rs_list, dot_pos, user_id):
    cur_user = user_list[user_id]
    cur_dot = dot_pos[:, user_id]

    while cur_user.relationships_1:
        delete_rs(rs_list, cur_user.relationships_1[0].id)
    while cur_user.relationships_2:
        delete_rs(rs_list, cur_user.relationships_2[0].id)

    cur_user.id = None
    user_list[user_id] = None

    return cur_user, cur_dot
