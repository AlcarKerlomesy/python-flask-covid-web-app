class _StateGenerator:
    def __init__(self):
        self.bit_pos = 0

    def get_next_state(self, num_bits):
        mask = (1 << num_bits) - 1 if num_bits > 1 else 1
        state_bit = (mask << self.bit_pos, self.bit_pos)
        self.bit_pos += num_bits
        return state_bit


# Constant
max_distance = 3
max_days_active = 15

# User state
_user_state = _StateGenerator()

HAS_COVID =         _user_state.get_next_state(1)
IS_SICK =           _user_state.get_next_state(1)
HAS_RECOVERED =     _user_state.get_next_state(1)
IS_VACCINATED =     _user_state.get_next_state(1)
COVID_DISTANCE =    _user_state.get_next_state(2)
SICK_DISTANCE =     _user_state.get_next_state(2)

HAS_COVID_TEMP =    _user_state.get_next_state(1)
IS_SICK_TEMP =      _user_state.get_next_state(1)

# Relationship state
_rs_state = _StateGenerator()

RS_TO_DELETE =      _rs_state.get_next_state(1)
RS_INACTIVE =       _rs_state.get_next_state(1)
RS_REG_CONTACT =    _rs_state.get_next_state(1)


def get_state_bool(instance_state, state):
    return bool(instance_state & state[0])


def get_state_int(instance_state, state):
    return (instance_state & state[0]) >> state[1]


def set_user_state(user, state, value):
    user.user_state = (user.user_state - (user.user_state & state[0])) | (value << state[1])


def set_rs_state(rs, state, value):
    rs.rs_state = (rs.rs_state - (rs.rs_state & state[0])) | (value << state[1])
