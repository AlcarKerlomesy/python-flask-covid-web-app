from .state_format import max_distance, set_user_state, get_state_bool, get_state_int
from .get_contacts import get_active_contacts


def update_all_distances(main_user, init_distance, update_user, state_bit, distance_bits):
    """Update the contacts (at a distance of distance + 1).
    Then, the contacts of the contatct (at a distance of distance + 2), and so forth.
    The user "main_user" is not updated

    get_update_users(user, distance, state_bit, distance_bits): This function is called when the algorithm
    proposes to update the user "user". The output is a boolean (or equivalent) which determines if this user is taken
    into account in the next iteration."""

    def _update_all_distances(user, distance):
        user_list = get_active_contacts(user)
        gen_list = [None] * len(user_list)

        # Update layer "distance" and get the generators
        gen_id = 0
        for cur_user in user_list:
            if update_user(cur_user, distance, state_bit, distance_bits):
                if distance < max_distance:
                    # noinspection PyTypeChecker
                    gen_list[gen_id] = _update_all_distances(cur_user, distance + 1)
                    gen_id += 1

        gen_list = gen_list[:gen_id]

        # Update layer "distance + 1" to "max_dist"
        for _ in range(distance, max_distance):
            yield
            for cur_gen in gen_list:
                # noinspection PyTypeChecker
                next(cur_gen)

        yield

    main_gen = _update_all_distances(main_user, init_distance + 1)

    for _ in range(init_distance, max_distance):
        next(main_gen)


# An useful function to get a more meaningful distance
def get_modified_distance(user_state, state_bit, distance_bits):
    if get_state_bool(user_state, state_bit):
        return 0
    if get_state_int(user_state, distance_bits) == 0:
        return max_distance + 1
    return get_state_int(user_state, distance_bits)


# Main candidates of the "get_update_users" function
def update_user_sick(user, distance, state_bit, distance_bits):
    user_distance = get_state_int(user.user_state, distance_bits)
    if not get_state_bool(user.user_state, state_bit) and \
            (user_distance == 0 or user_distance > distance):
        set_user_state(user, distance_bits, distance)
        return True

    return False


def update_user_recover(user, distance, state_bit, distance_bits):
    if not get_state_bool(user.user_state, state_bit) and get_state_int(user.user_state, distance_bits) == distance:
        cur_dist = 0 if distance + 2 > max_distance else distance + 2

        for cur_contact in get_active_contacts(user):
            contact_dist = get_state_int(cur_contact.user_state, distance_bits)
            if get_state_bool(cur_contact.user_state, state_bit) or (distance > 1 and contact_dist == distance - 1):
                return False
            if max_distance > distance == contact_dist and (cur_dist == 0 or cur_dist > distance + 1):
                cur_dist = distance + 1

        set_user_state(user, distance_bits, cur_dist)
        return True

    return False


# Top-level functions
def make_user_sick(user, state_bit, distance_bits):
    set_user_state(user, state_bit, 1)
    update_all_distances(user, 0, update_user_sick, state_bit, distance_bits)


def make_user_recover(user, state_bit, distance_bits):
    set_user_state(user, state_bit, 0)

    # Assuming that a contact is sick
    set_user_state(user, distance_bits, 1)
    # Check contacts and increase the distance accordingly
    update_user_recover(user, 1, state_bit, distance_bits)

    update_all_distances(user, 0, update_user_recover, state_bit, distance_bits)


def deactivate_user(user, state_bit, distance_bits):
    start_dist = get_modified_distance(user.user_state, state_bit, distance_bits)

    if start_dist < max_distance:
        # Make it invisible
        set_user_state(user, state_bit, 0)
        set_user_state(user, distance_bits, 0)

        # Check contacts and increase the distance accordingly
        update_all_distances(user, start_dist, update_user_recover, state_bit, distance_bits)


# It is slightly better to [create the relationship/set the RS_INACTIVE flag to FALSE] after calling this function
def activate_relationship(user_1, user_2, state_bit, distance_bits):
    dist_1 = get_modified_distance(user_1.user_state, state_bit, distance_bits)
    dist_2 = get_modified_distance(user_2.user_state, state_bit, distance_bits)

    main_user = None
    starting_dist = None
    if dist_1 > dist_2 + 1:
        main_user = user_1
        starting_dist = dist_2 + 1
    elif dist_2 > dist_1 + 1:
        main_user = user_2
        starting_dist = dist_1 + 1

    if main_user:
        set_user_state(main_user, distance_bits, starting_dist)
        if starting_dist < max_distance:
            update_all_distances(main_user, starting_dist, update_user_sick, state_bit, distance_bits)


# The RS_INACTIVE flag must be set to TRUE before calling this function
def deactivate_relationship(user_1, user_2, state_bit, distance_bits):
    dist_1 = get_modified_distance(user_1.user_state, state_bit, distance_bits)
    dist_2 = get_modified_distance(user_2.user_state, state_bit, distance_bits)

    cur_user = None
    cur_dist = None
    if dist_1 == dist_2 + 1:
        cur_user = user_1
        cur_dist = dist_1
    elif dist_2 == dist_1 + 1:
        cur_user = user_2
        cur_dist = dist_2

    if cur_user:
        update_user_recover(cur_user, cur_dist, state_bit, distance_bits)
        update_all_distances(cur_user, cur_dist, update_user_recover, state_bit, distance_bits)
