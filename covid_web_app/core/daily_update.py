from datetime import date
from .state_format import *
from .update_distances import make_user_recover, deactivate_relationship

__all__ = ['daily_update']


def daily_update(user_list, rs_list, date_today=None):

    if date_today is None:
        date_today = date.today()

    user_to_delete = []

    for cur_user in user_list:
        for state_bit, state_bit_temp, distance_bits in [[HAS_COVID, HAS_COVID_TEMP, COVID_DISTANCE],
                                                         [IS_SICK, IS_SICK_TEMP, SICK_DISTANCE]]:
            if get_state_bool(cur_user.user_state, state_bit_temp):
                set_user_state(cur_user, state_bit_temp, 0)
                make_user_recover(cur_user, state_bit, distance_bits)

    rs_to_delete = []

    for cur_rs in rs_list:
        if not get_state_bool(cur_rs.rs_state, RS_INACTIVE) and (
                get_state_bool(cur_rs.rs_state, RS_TO_DELETE) or
                (not get_state_bool(cur_rs.rs_state, RS_REG_CONTACT) and (date_today - cur_rs.last_time).days > max_days_active)
        ):
            set_rs_state(cur_rs, RS_INACTIVE, 1)
            for state_bit, distance_bits in [[HAS_COVID, COVID_DISTANCE], [IS_SICK, SICK_DISTANCE]]:
                deactivate_relationship(cur_rs.user_1, cur_rs.user_2, state_bit, distance_bits)

        if get_state_bool(cur_rs.rs_state, RS_TO_DELETE):
            rs_to_delete.append(cur_rs)

    return user_to_delete, rs_to_delete
