from .state_format import *


def get_active_contacts(user):
    user_list = []

    for cur_rs in user.relationships_1:
        if not get_state_bool(cur_rs.rs_state, RS_INACTIVE):
            user_list.append(cur_rs.user_2)

    for cur_rs in user.relationships_2:
        if not get_state_bool(cur_rs.rs_state, RS_INACTIVE):
            user_list.append(cur_rs.user_1)

    return user_list
