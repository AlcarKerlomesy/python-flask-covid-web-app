from .state_format import *
from .update_distances import deactivate_user, activate_relationship
from ..models import User, Relationship

__all__ = ['create_user', 'delete_user', 'create_relationship', 'delete_relationship']


def create_user(username, email, postal_code, hashed_password, parent=None):
    if parent:
        return User(username=username, email=email, postal_code=postal_code,
                    password=hashed_password, user_state=0, parent=parent)
    else:
        return User(username=username, email=email, postal_code=postal_code, password=hashed_password, user_state=0)


def delete_single_user(user):
    for state_bit, distance_bits in [[HAS_COVID, COVID_DISTANCE], [IS_SICK, SICK_DISTANCE]]:
        deactivate_user(user, state_bit, distance_bits)


def delete_user(user, children_list):
    for cur_child in children_list:
        delete_single_user(cur_child)
    delete_single_user(user)
    return user


def create_relationship(user_1, user_2):
    for state_bit, distance_bits in [[HAS_COVID, COVID_DISTANCE], [IS_SICK, SICK_DISTANCE]]:
        activate_relationship(user_1, user_2, state_bit, distance_bits)

    return Relationship(user_1=user_1, user_2=user_2, rs_state=0)


def delete_relationship(relationship):
    set_rs_state(relationship, RS_TO_DELETE, 1)

    return None
