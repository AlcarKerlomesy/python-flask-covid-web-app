from datetime import date
from .state_format import *
from .update_distances import activate_relationship

__all__ = ['set_meeting_day_now', 'set_regular_contact']


def set_meeting_day_now(rs):
    rs.last_time = date.today()
    if get_state_bool(rs.rs_state, RS_INACTIVE):
        for state_bit, distance_bits in [[HAS_COVID, COVID_DISTANCE], [IS_SICK, SICK_DISTANCE]]:
            activate_relationship(rs.user_1, rs.user_2, state_bit, distance_bits)
        set_rs_state(rs, RS_INACTIVE, 0)


def set_regular_contact(rs, is_regular):
    if is_regular and not get_state_bool(rs.rs_state, RS_REG_CONTACT):
        set_rs_state(rs, RS_REG_CONTACT, 1)
        if get_state_bool(rs.rs_state, RS_INACTIVE):
            for state_bit, distance_bits in [[HAS_COVID, COVID_DISTANCE], [IS_SICK, SICK_DISTANCE]]:
                activate_relationship(rs.user_1, rs.user_2, state_bit, distance_bits)
            set_rs_state(rs, RS_INACTIVE, 0)
    elif not is_regular and get_state_bool(rs.rs_state, RS_REG_CONTACT):
        set_rs_state(rs, RS_REG_CONTACT, 0)
        rs.last_time = date.today()
