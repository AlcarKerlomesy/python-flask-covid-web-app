from .state_format import *
from .update_distances import make_user_sick

__all__ = ['user_get_info', 'user_set_state', 'rs_get_info']


class UserInfo:
    def __init__(self, user_state):
        self.has_covid = get_state_bool(user_state, HAS_COVID)
        self.is_sick = get_state_bool(user_state, IS_SICK)
        self.has_recovered = get_state_bool(user_state, HAS_RECOVERED)
        self.is_vaccinated = get_state_bool(user_state, IS_VACCINATED)

        self.covid_distance = get_state_int(user_state, COVID_DISTANCE)
        self.sick_distance = get_state_int(user_state, SICK_DISTANCE)

        self.has_covid_temp = get_state_bool(user_state, HAS_COVID_TEMP)
        self.is_sick_temp = get_state_bool(user_state, IS_SICK_TEMP)


class RsInfo:
    def __init__(self, rs_state):
        self.soon_deleted = get_state_bool(rs_state, RS_TO_DELETE)
        self.inactive = get_state_bool(rs_state, RS_INACTIVE)
        self.regular_contact = get_state_bool(rs_state, RS_REG_CONTACT)


def user_get_info(user_state):
    return UserInfo(user_state)


def rs_get_info(rs_state):
    return RsInfo(rs_state)


def user_set_state(user, has_covid, is_sick, has_recovered, is_vaccinated):

    for state_bit, state_bit_temp, distance_bits, next_state in [[HAS_COVID, HAS_COVID_TEMP, COVID_DISTANCE, has_covid],
                                                                 [IS_SICK, IS_SICK_TEMP, SICK_DISTANCE, is_sick]]:
        if get_state_bool(user.user_state, state_bit):
            set_user_state(user, state_bit_temp, not next_state)
        elif next_state:
            make_user_sick(user, state_bit, distance_bits)

    set_user_state(user, HAS_RECOVERED, has_recovered)
    set_user_state(user, IS_VACCINATED, is_vaccinated)
