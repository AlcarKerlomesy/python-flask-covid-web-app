from flask import g
from wtforms.validators import Length, NumberRange, ValidationError
from .models import User, Relationship

# The max length should be equal (or smaller) that the allowed string length of the related field in models.py
username_length = Length(min=2, max=20)
email_length = Length(max=50)


def email_unique(form, email):
    user = User.query.filter_by(email=email.data).first()
    if user:
        raise ValidationError('That email is taken. Please choose a different one.')


postal_code_range = NumberRange(min=1000, max=9999, message="Invalid postal code")


def check_username_id(user_id_field_name):

    def _check(form, username):
        user_id = form[user_id_field_name].data
        if User.query.get(user_id) is None or username.data != User.query.get(user_id).username:
            raise ValidationError("The username doesn't correspond to the account ID.")

    return _check


def check_contact_duplicate(form, user_id):

    if g.cur_user.id == user_id.data:
        raise ValidationError("You cannot add yourself as a contact.")

    if user_id.data in [cur_rs.user_2_id for cur_rs in g.cur_user.relationships_1] +\
            [cur_rs.user_1_id for cur_rs in g.cur_user.relationships_2]:
        raise ValidationError("This account is already in your contact list.")
