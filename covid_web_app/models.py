from datetime import date
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired, BadSignature
from flask import current_app
from flask_login import UserMixin
from . import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    postal_code = db.Column(db.Integer, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    user_state = db.Column(db.Integer, nullable=False)

    parent_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    relationships_1 = db.relationship('Relationship', backref='user_1',
                                      lazy=True, foreign_keys='Relationship.user_1_id', cascade="all")
    relationships_2 = db.relationship('Relationship', backref='user_2',
                                      lazy=True, foreign_keys='Relationship.user_2_id', cascade="all")
    children = db.relationship('User', backref=db.backref('parent', remote_side=id), lazy=True, cascade="all")

    def get_reset_token(self, expires_sec=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except (SignatureExpired, BadSignature):
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.postal_code}', '{self.user_state}')"


class Relationship(db.Model):
    __tablename__ = 'relationship'
    id = db.Column(db.Integer, primary_key=True)
    last_time = db.Column(db.Date, nullable=False, default=date.today)
    rs_state = db.Column(db.Integer, nullable=False)

    user_1_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user_2_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return f"Relationship('{self.user_1.username}', '{self.user_2.username}', '{self.rs_state}')"
