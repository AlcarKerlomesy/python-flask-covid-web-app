from flask import render_template, Blueprint, redirect, url_for, request
from flask_login import login_required
from datetime import date
from .forms import DailyUpdateForm
from .. import db
from ..models import User, Relationship
from ..core import daily_update as core_daily_update

main = Blueprint('main', __name__)


@main.route("/")
def home():
    return render_template('main/home.html')


@main.route("/privacy")
def privacy():
    return render_template('main/privacy.html', title='Privacy')


@main.route("/about")
def about():
    return render_template('main/about.html', title='About')


# Only for the development version
@main.route("/a_day_passes", methods=['GET', 'POST'])
@login_required
def daily_update():
    update_form = DailyUpdateForm()

    if request.method == 'POST' and update_form.validate_on_submit():
        user_to_delete, rs_to_delete = core_daily_update(User.query.all(), Relationship.query.all(), update_form.today_date.data)
        for cur_user in user_to_delete:
            db.session.delete(cur_user)
        for cur_rs in rs_to_delete:
            db.session.delete(cur_rs)
        db.session.commit()

        return redirect(url_for('relationships.user_contacts'))
    elif request.method == 'GET':
        update_form.today_date.data = date.today()

    return render_template('main/a_day_passes.html', title='Daily update', update_form=update_form)
