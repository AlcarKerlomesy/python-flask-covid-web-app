from flask_wtf import FlaskForm
from wtforms import DateField, SubmitField


class DailyUpdateForm(FlaskForm):
    today_date = DateField("Today's date")
    do_daily_update = SubmitField("Do the daily update")
