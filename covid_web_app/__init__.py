from flask import Flask, g, session
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager, current_user
from flask_mail import Mail
from .config import Config

db = SQLAlchemy()
app_bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'
mail = Mail()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    db.create_all(app=app)
    app_bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    from .main.routes import main
    from .users.routes import users
    from .relationships.routes import relationships

    app.register_blueprint(main)
    app.register_blueprint(users)
    app.register_blueprint(relationships)

    from .models import User

    @app.before_request
    def set_current_user():
        if current_user.is_authenticated:
            g.cur_user = User.query.get(session['cur_user_id'] if 'cur_user_id' in session else current_user.id)
            if 'have_parent_access' not in session:
                user = g.cur_user
                if user == current_user:
                    session['have_parent_access'] = False
                else:
                    while user != current_user and user.parent:
                        user = user.parent
                    session['have_parent_access'] = user == current_user
            g.have_parent_access = session['have_parent_access']
        else:
            g.cur_user = None
            g.have_parent_access = False

    return app


def manage_db(db_action, config_class=Config):
    """db_action: function with the database as input"""
    with create_app(config_class).app_context():
        from .models import User, Relationship
        db_action(db)
    return
