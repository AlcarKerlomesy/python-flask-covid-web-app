from flask_wtf import FlaskForm
from wtforms import BooleanField, SubmitField, IntegerField, StringField
from wtforms.validators import DataRequired
from ..form_validator import username_length, check_username_id, check_contact_duplicate


class UserStateUpdateForm(FlaskForm):
    has_covid = BooleanField('You are positive to COVID')
    is_sick = BooleanField("You are feeling sick but it may not be COVID")
    has_recovered = BooleanField('You got COVID and you recovered')
    is_vaccinated = BooleanField('You are vaccinated against COVID')
    user_submit = SubmitField('Update status')


class CreateRelationshipForm(FlaskForm):
    user_id = IntegerField('Account ID', validators=[DataRequired(), check_contact_duplicate])
    username = StringField('Username', validators=[DataRequired(), username_length, check_username_id('user_id')])
    regular_contact = BooleanField('I will see him/her every few days')
    rs_submit = SubmitField('Add contact')


class ChangeMeetingTimeForm(FlaskForm):
    refresh_time = BooleanField('I met him/her today')
    regular_contact = BooleanField('I see him/her every few days')
    form_submit = SubmitField('Submit')
