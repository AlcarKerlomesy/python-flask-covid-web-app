from flask import Blueprint, render_template, g, request, abort, url_for, redirect
from flask_login import login_required
from sqlalchemy import or_
from .forms import UserStateUpdateForm, CreateRelationshipForm, ChangeMeetingTimeForm
from .message import user_state_message, get_contact_info, get_meeting_time_string
from .. import db
from ..models import User, Relationship
from ..core import create_relationship, delete_relationship, user_get_info, user_set_state, rs_get_info,\
    set_meeting_day_now, set_regular_contact

relationships = Blueprint('relationships', __name__)


@relationships.route("/user_contacts", methods=['GET', 'POST'])
@login_required
def user_contacts():
    user_form = UserStateUpdateForm()
    rs_form = CreateRelationshipForm()
    show_rs_modal = False
    if request.method == 'POST':

        if user_form.user_submit.data and user_form.validate_on_submit():
            user_set_state(g.cur_user,
                           user_form.has_covid.data, user_form.is_sick.data,
                           user_form.has_recovered.data, user_form.is_vaccinated.data)
            db.session.commit()

        if rs_form.rs_submit.data:
            if rs_form.validate_on_submit():
                other_user = User.query.get(rs_form.user_id.data)
                new_rs = create_relationship(user_1=g.cur_user, user_2=other_user)
                set_regular_contact(new_rs, rs_form.regular_contact.data)
                db.session.add(new_rs)
                db.session.commit()

                rs_form.user_id.data = None
                rs_form.username.data = None

            elif rs_form.is_submitted():
                show_rs_modal = True

    elif request.method == 'GET':
        user_info = user_get_info(g.cur_user.user_state)
        user_form.has_covid.data = user_info.has_covid
        user_form.is_sick.data = user_info.is_sick
        user_form.has_recovered.data = user_info.has_recovered
        user_form.is_vaccinated.data = user_info.is_vaccinated

        rs_form.regular_contact.data = False

    user_message = user_state_message(g.cur_user.user_state)

    contact_list = db.session.query(User, Relationship).\
        filter(or_(Relationship.user_1_id == g.cur_user.id, Relationship.user_2_id == g.cur_user.id)).\
        join(User, or_(User.id == Relationship.user_1_id, User.id == Relationship.user_2_id)).\
        filter(User.id != g.cur_user.id).\
        order_by(User.username).all()

    contact_info = (get_contact_info(g.cur_user.user_state, cur_contact[0].user_state, cur_contact[1].rs_state)
                    for cur_contact in contact_list)

    return render_template("relationships/user_contacts.html", title="User & Contacts", user_message=user_message,
                           user_form=user_form, rs_form=rs_form, show_rs_modal=show_rs_modal,
                           contact_list_info=zip(contact_list, contact_info))


@relationships.route("/contact/<int:rs_id>", methods=['GET', 'POST'])
@login_required
def contact_detail(rs_id):
    cur_rs = Relationship.query.get(rs_id)

    cur_contact = None
    if cur_rs:
        if cur_rs.user_1 == g.cur_user:
            cur_contact = cur_rs.user_2
        elif cur_rs.user_2 == g.cur_user:
            cur_contact = cur_rs.user_1

    if cur_contact is None:
        abort(403)

    meeting_form = ChangeMeetingTimeForm()

    if request.method == 'POST':
        if meeting_form.validate_on_submit():
            set_regular_contact(cur_rs, meeting_form.regular_contact.data)
            if not meeting_form.regular_contact.data and meeting_form.refresh_time.data:
                set_meeting_day_now(cur_rs)
            db.session.commit()

    rs_info = rs_get_info(cur_rs.rs_state)
    meeting_form.regular_contact.data = rs_info.regular_contact
    meeting_form.refresh_time.data = False

    contact_info = get_contact_info(g.cur_user.user_state, cur_contact.user_state, cur_rs.rs_state)
    last_meeting_str = get_meeting_time_string(cur_rs)

    return render_template("relationships/contact_detail.html", title="Contact", cur_contact=cur_contact, rs_id=cur_rs.id,
                           contact_info=contact_info, last_meeting_str=last_meeting_str, meeting_form=meeting_form)


@relationships.route("/delete_contact/<int:rs_id>", methods=['POST'])
@login_required
def delete_contact(rs_id):
    del_rs = Relationship.query.get(rs_id)
    if not (del_rs and (del_rs.user_1 == g.cur_user or del_rs.user_2 == g.cur_user)):
        abort(403)

    del_rs = delete_relationship(del_rs)
    if del_rs:
        db.session.delete(del_rs)
    db.session.commit()

    return redirect(url_for('relationships.user_contacts'))
