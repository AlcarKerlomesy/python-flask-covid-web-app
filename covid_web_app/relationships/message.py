from datetime import date
from ..core import user_get_info, rs_get_info


def user_state_message(user_state):
    output_message = []
    temp_message = []

    user_info = user_get_info(user_state)

    if user_info.has_covid:
        temp_message.append("got the COVID")
    if user_info.is_sick:
        temp_message.append("are not feeling well")
    if user_info.has_recovered:
        temp_message.append("recovered from COVID")
    if user_info.is_vaccinated:
        temp_message.append("are vaccinated")
    if user_info.covid_distance == 1:
        temp_message.append("had contact with someone with COVID")

    if temp_message:
        if len(temp_message) == 1:
            output_message.append(["You " + temp_message[0]])
        else:
            output_message.append(["You " + ", ".join(temp_message[:-1]) + " and " + temp_message[-1]])

    if user_info.has_covid:
        if user_info.is_sick:
            output_message[0].append(
                "Please stay at home and ask your friends or family members to bring foods and essential items. "
                "If the symptoms worsen or you can hold your breath for a time which is significantly shorter "
                "than usual, it is strongly advised to call a doctor as soon as possible."
            )
        else:
            output_message[0].append(
                "If you have been tested positive to COVID, you may still infect others even if you don't have "
                "any symptom. Therefore, please stay at home and ask your friends or family members to bring foods and "
                "essential items."
            )
    else:
        if user_info.covid_distance == 1:
            output_message[0].append(
                "Please stay at home and ask your friends or family members to bring foods and essential items. "
                "Normally, someone contacts you at the beginning of the quarantine and you should be asked to "
                "do a COVID test. At the end of the quarantine, you will be tested again."
            )
        else:
            if user_info.is_sick:
                output_message[0].append(
                    "If you're feeling sick, you should avoid social gathering, and if your symptoms match the ones of "
                    "the COVID, you should ask for a test and stay at home."
                )
            if user_info.has_recovered or user_info.is_vaccinated:
                output_message[0].append(
                    "You are partially immune to COVID and are much less likely to have complications than unvaccinated "
                    "people who are as healthy as you."
                )
            if user_info.covid_distance == 2:
                output_message.append(["One of your contact has a friend who has been infected by COVID"])
                output_message[-1].append(
                    "Normally, this contact is in quarantine. If you have some spare time, it would be nice if "
                    "you propose to help him to get what he needs."
                )
            if user_info.covid_distance == 3:
                output_message.append(["The COVID is getting close"])
                output_message[-1].append(
                    "You should not take any drastic measure since it is a friend of a friend of a friend who "
                    "has been infected by COVID. However, you should keep an eye on how the situation is evolving."
                )

    if not output_message:
        output_message.append(["Nobody around you has COVID, but you have no immunity against it."])
        output_message[0].append("It seems that you won't have COVID if you stay with the people that you know.")

    if user_info.has_covid_temp:
        output_message.append(["You are healed from COVID"])
        if user_info.is_sick_temp:
            output_message[-1].append("Others will still see you as infected by COVID and sick. This status will "
                                      "change tomorrow.")
        else:
            output_message[-1].append("Others will still see you as infected by COVID. This status will "
                                      "change tomorrow.")
    elif user_info.is_sick_temp:
        output_message.append(["You are no more sick"])
        output_message[-1].append("Others will still see you as sick. This status will "
                                  "change tomorrow.")

    return output_message


class ContactSymbol:
    def __init__(self, s_class, s_style, description):
        self.s_class = s_class
        self.s_style = s_style
        self.description = description


def get_contact_info(user_state, contact_state, rs_state):
    contact_info = []

    user_info = user_get_info(user_state)
    contact_info_b = user_get_info(contact_state)
    rs_info = rs_get_info(rs_state)

    if rs_info.soon_deleted:
        contact_info.append(ContactSymbol("fas fa-times", "color:red", "Will soon be removed from your contact list"))
    if rs_info.regular_contact:
        contact_info.append(ContactSymbol("fas fa-infinity", "color:green", "You have regular contact with him/her"))
    elif rs_info.inactive:
        contact_info.append(ContactSymbol("fas fa-hourglass-end", "color:blue", "You haven't see him/her for more than two weeks"))
    if contact_info_b.is_vaccinated:
        contact_info.append(ContactSymbol("fas fa-syringe", "color:green", "Vaccinated"))
    if contact_info_b.has_recovered:
        contact_info.append(ContactSymbol("fas fa-virus-slash", "color:green", "Got COVID and has recovered"))
    if contact_info_b.has_covid:
        contact_info.append(ContactSymbol("fas fa-viruses", "color:red", "Positive to COVID"))
    elif user_info.covid_distance == 0 or contact_info_b.covid_distance <= user_info.covid_distance:
        if contact_info_b.covid_distance == 1:
            contact_info.append(ContactSymbol("fas fa-dice-one", "color:red",
                                            "At least one of his/her contact has COVID"))
        elif contact_info_b.covid_distance == 2:
            contact_info.append(ContactSymbol("fas fa-dice-two", "color:red",
                                            "At least one of his/her contact knows someone who has COVID"))
    if contact_info_b.is_sick:
        contact_info.append(ContactSymbol("fas fa-viruses", "color:orange", "Is sick, but it may not be COVID"))
    elif user_info.sick_distance == 0 or contact_info_b.sick_distance <= user_info.sick_distance:
        if contact_info_b.sick_distance == 1:
            contact_info.append(ContactSymbol("fas fa-dice-one", "color:orange",
                                            "At least one of his/her contact is sick (may not be COVID)"))
        elif contact_info_b.sick_distance == 2:
            contact_info.append(ContactSymbol("fas fa-dice-two", "color:orange",
                                            "At least one of his/her contact knows someone who is sick (may not be COVID)"))

    return contact_info


def get_meeting_time_string(rs):
    rs_info = rs_get_info(rs.rs_state)
    if rs_info.regular_contact:
        return "You have regular contact with him/her"
    else:
        delta_days = (date.today() - rs.last_time).days
        if delta_days == 0:
            return rs.last_time.strftime("You met him/her today (%A %B %d %Y)")
        if delta_days == 1:
            return rs.last_time.strftime("You met him/her yesterday (%A %B %d %Y)")
        return "You met him/her " + str(delta_days) + rs.last_time.strftime(" days ago, on %A %B %d %Y")
