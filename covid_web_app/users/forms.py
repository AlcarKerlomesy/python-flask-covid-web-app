from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, IntegerField, BooleanField
from wtforms.validators import DataRequired, Email, EqualTo, Optional
from ..form_validator import username_length, email_length, email_unique, postal_code_range


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), username_length])
    email = StringField('Email', validators=[DataRequired(), Email(), email_length, email_unique])
    postal_code = IntegerField('Postal code (numeric only)', validators=[DataRequired(), postal_code_range])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')


class LoginForm(FlaskForm):
    email = StringField('Email or Account ID if no email address', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class RequestResetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Request')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')


class UpdateAccountForm(FlaskForm):
    username = StringField('Username', validators=[Optional(strip_whitespace=False), username_length])
    email = StringField('Email', validators=[Optional(strip_whitespace=False), Email(), email_length])
    postal_code = IntegerField('Postal code', validators=[Optional(strip_whitespace=False), postal_code_range])
    password = PasswordField('Password')
    confirm_password = PasswordField('Confirm Password', validators=[EqualTo('password')])
    submit = SubmitField('Update')

    def validate_email(self, email):
        if email.data != current_user.email:
            email_unique(self, email)


class CreateSubAccountForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), username_length])
    email = StringField('Email (Optional)', validators=[Optional(strip_whitespace=False), Email(),
                                                        email_length, email_unique])
    postal_code = IntegerField('Postal code (Numeric only)', validators=[DataRequired(), postal_code_range])
    password = PasswordField('Password (Optional)', validators=[Optional(strip_whitespace=False)])
    confirm_password = PasswordField('Confirm Password', validators=[EqualTo('password')])
    submit = SubmitField('Create account')
