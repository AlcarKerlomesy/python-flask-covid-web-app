from flask import render_template, Blueprint, url_for, flash, redirect, abort, request, session, g, current_app
from flask_login import login_user, current_user, logout_user, login_required
from .forms import RegistrationForm, LoginForm, RequestResetForm, ResetPasswordForm,\
    UpdateAccountForm, CreateSubAccountForm
from .utils import send_reset_email, get_children_recursive
from .. import db, app_bcrypt
from ..models import User
from ..config import python_ver
from ..core import create_user, delete_user

users = Blueprint('users', __name__)


@users.route("/register", methods=['GET', 'POST'])
def register():
    if g.cur_user:
        return redirect(url_for('relationships.user_contacts'))

    form = RegistrationForm()
    if form.validate_on_submit():
        if python_ver == 3.9:
            hashed_password = app_bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        else:
            hashed_password = app_bcrypt.generate_password_hash(form.password.data)
        new_user = create_user(username=form.username.data, email=form.email.data,
                               postal_code=form.postal_code.data, hashed_password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in.', 'success')
        return redirect(url_for('users.login'))

    return render_template('users/register.html', title='Register', form=form)


@users.route("/login", methods=['GET', 'POST'])
def login():
    if g.cur_user:
        return redirect(url_for('relationships.user_contacts'))

    form = LoginForm()
    if form.validate_on_submit():
        if form.email.data.isnumeric():
            user = User.query.get(form.email.data)
            if user.email:
                user = None
        else:
            user = User.query.filter_by(email=form.email.data).first()
        if user and app_bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('relationships.user_contacts'))

        if user and not user.password:
            flash('You can access your account only through your parent account.', 'info')
        else:
            flash('Login Unsuccessful. Please check email and password.', 'danger')

    return render_template('users/login.html', title='Login', form=form)


@users.route("/logout")
def logout():
    logout_user()
    session.pop('cur_user_id', None)
    session.pop('have_parent_access', None)

    return redirect(url_for('main.home'))


@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if g.cur_user:
        return redirect(url_for('relationships.user_contacts'))
    if '@' not in current_app.config['MAIL_USERNAME']:
        flash("This web app didn't configure an email address.", 'danger')
        return redirect(url_for('users.login'))

    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent with instructions to reset your password.', 'info')
        return redirect(url_for('users.login'))

    return render_template('users/reset_request.html', title='Reset Password', form=form)


@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if g.cur_user:
        return redirect(url_for('relationships.user_contacts'))

    user = User.verify_reset_token(token)

    if user is None:
        flash('That is an invalid or expired token.', 'warning')
        return redirect(url_for('users.reset_request'))

    form = ResetPasswordForm()
    if form.validate_on_submit():
        if python_ver == 3.9:
            hashed_password = app_bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        else:
            hashed_password = app_bcrypt.generate_password_hash(form.password.data)
        user.password = hashed_password
        db.session.commit()
        flash('Your password has been updated! You are now able to log in.', 'success')
        return redirect(url_for('users.login'))

    return render_template('users/reset_token.html', title='Reset Password', form=form)


@users.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.username.data:          g.cur_user.username = form.username.data
        if form.email.data:             g.cur_user.email = form.email.data
        if form.postal_code.data:       g.cur_user.postal_code = form.postal_code.data
        if form.password.data:
            if python_ver == 3.9:
                hashed_password = app_bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            else:
                hashed_password = app_bcrypt.generate_password_hash(form.password.data)
            g.cur_user.password = hashed_password
        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('users.account'))
    elif request.method == 'GET':
        form.username.data = g.cur_user.username
        form.email.data = g.cur_user.email
        form.postal_code.data = g.cur_user.postal_code
        children_list = get_children_recursive(g.cur_user)

    return render_template('users/account.html', title='Account', form=form, children_list=children_list)


@users.route("/delete_account", methods=['POST'])
@login_required
def delete_account():
    if g.cur_user == current_user:
        logout_user()
        flash('Your account has been deleted!', 'info')
    else:
        session['cur_user_id'] = g.cur_user.parent_id
        flash("'" + g.cur_user.username + "' account has been deleted!", 'info')

    session.pop('have_parent_access', None)
    deleted_user = delete_user(g.cur_user, get_children_recursive(g.cur_user))
    if deleted_user:
        db.session.delete(deleted_user)
        db.session.commit()

    return redirect(url_for('main.home'))


@users.route("/sub_accounts")
@login_required
def sub_accounts():
    return render_template('users/sub_accounts.html', title='Sub-accounts',
                           get_children_recursive=get_children_recursive)


@users.route("/create_sub_account", methods=['GET', 'POST'])
@login_required
def create_sub_account():
    form = CreateSubAccountForm()
    if form.validate_on_submit():
        if form.password.data:
            if python_ver == 3.9:
                hashed_password = app_bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            else:
                hashed_password = app_bcrypt.generate_password_hash(form.password.data)
        else:
            hashed_password = ''
        new_user = create_user(username=form.username.data, email=form.email.data, postal_code=form.postal_code.data,
                               hashed_password=hashed_password, parent=g.cur_user)
        db.session.add(new_user)
        db.session.commit()
        flash("'" + new_user.username + "' account has been created!", 'success')
        return redirect(url_for('users.sub_accounts'))

    return render_template('users/create_sub_account.html', title='Create Sub-Account', form=form)


@users.route("/go_to_sub_account/<int:child_id>")
@login_required
def got_to_sub_account(child_id):
    if child_id < 0 or child_id >= len(g.cur_user.children):
        abort(403)

    session['cur_user_id'] = g.cur_user.children[child_id].id
    session['have_parent_access'] = True

    return redirect(url_for('relationships.user_contacts'))


@users.route("/go_to_parent_account")
@login_required
def go_to_parent_account():
    if not g.have_parent_access:
        abort(403)

    session['cur_user_id'] = g.cur_user.parent.id
    session.pop('have_parent_access', None)

    return redirect(url_for('relationships.user_contacts'))


@users.route("/delete_sub_account/<int:child_id>", methods=['POST'])
@login_required
def delete_sub_account(child_id):
    if child_id < 0 or child_id >= len(g.cur_user.children):
        abort(403)

    deleted_user = g.cur_user.children[child_id]
    deleted_user = delete_user(deleted_user, get_children_recursive(deleted_user))
    if deleted_user:
        db.session.delete(deleted_user)
        db.session.commit()

    return redirect(url_for('users.sub_accounts'))


@users.route("/detach_sub_account/<int:child_id>", methods=['POST'])
@login_required
def detach_sub_account(child_id):
    if child_id < 0 or child_id >= len(g.cur_user.children):
        abort(403)

    cur_child = g.cur_user.children[child_id]
    if cur_child.email and cur_child.password:
        cur_child.parent_id = None
        db.session.commit()

    return redirect(url_for('users.sub_accounts'))
