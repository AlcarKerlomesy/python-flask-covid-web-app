from flask import current_app, url_for
from flask_mail import Message
from .. import mail, db
from ..models import User


def send_reset_email(user):
    token = user.get_reset_token(600)
    msg = Message('Password Reset Request', sender=current_app.config['MAIL_USERNAME'], recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
{url_for('users.reset_token', token=token, _external=True)}

If you did not make this request then simply ignore this email and no change will be made.
'''
    mail.send(msg)


def get_children_recursive(user):
    start_recursive = User.query. \
        filter(User.id.in_([cur_children.id for cur_children in user.children])). \
        cte(recursive=True)
    return db.session.query(
            start_recursive.union_all(User.query.filter_by(parent_id=start_recursive.c.id))
        ).all()
