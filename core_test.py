from covid_web_app.test_core import load_scenario, plot_state

for user_list, rs_list, dot_pos in load_scenario[1]():
    plot_state(user_list, rs_list, dot_pos)
