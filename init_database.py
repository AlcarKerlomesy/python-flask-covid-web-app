from covid_web_app import manage_db


def init_db(db):
    db.create_all()


manage_db(init_db)
